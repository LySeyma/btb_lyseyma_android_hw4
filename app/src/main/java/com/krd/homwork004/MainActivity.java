package com.krd.homwork004;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    Button Go_Detail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
       Button btn_go_detail= findViewById(R.id.btn_go_detail);
        if (findViewById(R.id.layout_default) != null){
            getSupportFragmentManager().beginTransaction().add(R.id.my_frameLayout, new Home_Fragment()).commit();

            FragmentManager manager = this.getSupportFragmentManager();
            manager.beginTransaction()
                    .replace(R.id.my_frameLayout,new Home_Fragment()).commit();
        }
        if (findViewById(R.id.layout_land) != null){
            FragmentManager manager = this.getSupportFragmentManager();
            manager.beginTransaction()
                    .replace(R.id.my_frame,new Home_Fragment())
                    .replace(R.id.my_frameLayout,new DetailFragment()).commit();
        }
    }
}