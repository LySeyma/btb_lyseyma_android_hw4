package com.krd.homwork004;

public class List_Detail {

    public String title;

    public List_Detail(String title){
        this.title=title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
